import Vue from 'vue'
import App from './App'
import uView from "uview-ui";
Vue.use(uView);
Vue.config.productionTip = false

App.mpType = 'app'
//引入网络请求方法
import http, {
	URL
} from "./http/config.js"
Vue.prototype.$http = http;
Vue.prototype.$url = URL
//引入消息弹窗文件
import * as tool from "./utils/tip.js"
Vue.prototype.$tool = tool
console.log(tool)
const app = new Vue({
	...App
})

app.$mount()
