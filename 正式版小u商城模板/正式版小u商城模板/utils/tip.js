//定义并导出数据加载提示弹窗
export const load = (title) => {
	uni.showLoading({
		title
	})
	uni.showNavigationBarLoading()
}
//定义并导出关闭数据加载提示弹窗
export const hideLoad = () => {
	uni.hideLoading()
	uni.hideNavigationBarLoading()
}
//定义并导出消息加载提示弹窗
export const tip = (title, icon = "none") => {
	uni.showToast({
		title,
		icon
	})
}
