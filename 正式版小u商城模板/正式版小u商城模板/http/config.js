

// 1.引入基础模块

// 2.动态设置请求地址
let URL = ""
if(process.env.NODE_ENV == "development"){
	// 本地开发环境
	// URL = "http://localhost:3000"
	URL = "http://api.aslegou.top"
}else{
	// 线上正式的生成环境
	URL = "http://api.aslegou.top"
}

// 3.按需导出地址
export {URL};


/**
 * 4.定义网络请求方法并默认导出
 * 	options:{
		url:请求地址,
		data:{请求数据},
		method:请求方法 get | post（restfull风格）,
		header:{请求头}
	}
 */ 
export  default (options)=>{
	// 组装请求地址：
	let url = URL+'/api/'+options.url
	return new Promise((resolve,reject)=>{
		uni.request({
			url,
			method:options.method || 'get',
			data:options.data || {},
			header:options.header || {
				"content-type":"application/json"
			},
			success:res=>resolve(res.data),
			fail:err=>reject(err)
		})
	})
}


