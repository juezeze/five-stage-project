/* 后台数据接口
	1.设置响应头
	2.业务逻辑
	3.解决跨域
	
	正常授课过程中，可以先去使用路由模式进行编写项目，图表组件开发结束之后，切换成socket模式
*/
// 1.创建express实例对象
const Express = require("express");
const fs = require('fs');
const path = require("path")
const bodyParser = require("body-parser")
const cors = require("cors"); //解决跨域问题
const app = new Express();
// 1.设置响应头
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));
// 3.解决跨域问题
app.use(cors())
app.set('view engine', 'ejs');


// // 3.设置端口号
let server = app.listen(3000)

// ==================正常路由模式处理后端数据=================
// 2.业务逻辑
// 2.1各地区种植面积统计TOP10
app.get("/api/plantingAreaTop", (req, res) => {
	// 获取文件数据，返回给前端
	let fileUrl = path.join(__dirname, "data/plantingAreaTop.json");
	fs.readFile(fileUrl, function(err, data) {
		res.send(data.toString())
	});
})
// // 2.2全国种植收成情况
app.get("/api/plantingHarvest", (req, res) => {
	// 获取文件数据，返回给前端
	let fileUrl = path.join(__dirname, "data/plantingHarvest.json");
	fs.readFile(fileUrl, function(err, data) {
		res.send(data.toString())
	});
})
// // 2.3各地区农作物种植区域占比
app.get("/api/cropAreasProportion", (req, res) => {
	// 获取文件数据，返回给前端
	let fileUrl = path.join(__dirname, "data/cropAreasProportion.json");
	fs.readFile(fileUrl, function(err, data) {
		res.send(data.toString())
	});
})
// // 2.4全国农作物覆盖面积  地图
app.get("/api/CropCoverage", (req, res) => {
	// 获取文件数据，返回给前端
	let fileUrl = path.join(__dirname, "data/CropCoverage.json");
	fs.readFile(fileUrl, function(err, data) {
		res.send(data.toString())
	});
})
// // 2.5主要地区农作物增长趋势
app.get("/api/CropGrowthTrend", (req, res) => {
	// 获取文件数据，返回给前端
	let fileUrl = path.join(__dirname, "data/CropGrowthTrend.json");
	fs.readFile(fileUrl, function(err, data) {
		res.send(data.toString())
	});
})
// // 2.5农作物增长趋势
// app.get("/api/CropGrowthTrend", (req, res) => {
// 	// 获取文件数据，返回给前端
// 	let fileUrl = path.join(__dirname, "data/CropGrowthTrend.json");
// 	fs.readFile(fileUrl, function(err, data) {
// 		res.send(data.toString())
// 	});
// })
// // 2.6农作物总面积占比
app.get("/api/TotalAreaCropsProportion", (req, res) => {
	// 获取文件数据，返回给前端
	let fileUrl = path.join(__dirname, "data/TotalAreaCropsProportion.json");
	fs.readFile(fileUrl, function(err, data) {
		res.send(data.toString())
	});
})

// 2.6农作物总面积占比
// app.get("/api/area", (req, res) => {
// 	let areaname=req.query.areaname||'china'
// 	// 获取文件数据，返回给前端
// 	let fileUrl = path.join(__dirname, "data/map/"+areaname+".json");
// 	fs.readFile(fileUrl, function(err, data) {
// 		res.send(data.toString())
// 	});
// })
